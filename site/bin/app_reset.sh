#!/bin/bash

php bin/console doctrine:database:drop --if-exists --force --env=${1-dev}
php bin/console doctrine:database:create --env=${1-dev}
php bin/console doctrine:schema:create --env=${1-dev}
php bin/console hautelook_alice:doctrine:fixtures:load --env=${1-dev} --no-interaction -v

php bin/console cache:clear --env=${1-dev}
