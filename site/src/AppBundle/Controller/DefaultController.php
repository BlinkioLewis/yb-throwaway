<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Stuff\Stuff;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render(':default:index.html.twig');
    }

    /**
     * @Route("/list", name="listpage")
     */
    public function anotherAction()
    {
        return $this->render(':default:list.html.twig', [
            'stuffs' => $this->getDoctrine()->getRepository(Stuff::class)->findAll(),
        ]);
    }
}
