<?php

namespace AppBundle\Entity\Stuff;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Stuff
 *
 * @package AppBundle\Entity\Stuff
 * @author Lewis O'Connor <lewis@blinkio.co.uk>
 *
 * @ORM\Table(name="stuff")
 * @ORM\Entity
 */
class Stuff
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue("AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="desciop", type="string", length=255)
     */
    private $desc;

    /**
     * Get Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get Desc
     *
     * @return string
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * Set Desc
     *
     * @param string $desc
     * @return $this
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;

        return $this;
    }
}
