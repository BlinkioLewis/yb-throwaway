<?php

namespace AppBundle\DataFixtures\ORM;

use Hautelook\AliceBundle\Doctrine\DataFixtures\AbstractLoader;

/**
 * Class DataLoader
 *
 * @package AppBundle\DataFixtures\ORM\Dev
 * @author Dan Hopper <dan@blinkio.co.uk>
 */
class DataLoader extends AbstractLoader
{
    /**
     * {@inheritdoc}
     */
    public function getFixtures()
    {
        return [
            __DIR__.'/Fixtures/stuff.yml',
        ];
    }
}
